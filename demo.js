const toTree = (arr) => {
  const result = []
  const map = new Map()
  for (const item of arr) {
    map.set(item._id, { ...item, children: [] })
  }
  for (const item of arr) {
    const id = item._id
    const pid = item.pid
    if (map.get(pid)) {
      map.get(pid).children.push(map.get(id))
    } else {
      return result.push(map.get(id))
    }
  }

  return result
}
