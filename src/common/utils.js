import { JWT_SECRET } from '@/config'
import jwt from 'jsonwebtoken'

export const getJWTPayload = (token) => {
  console.log(token)
  return jwt.verify(token.split(' ')[1], JWT_SECRET)
}

// export const toTree = (items) => {
//   const result = []
//   const itemMap = {}

//   for (let item of items) {
//     item = item.toJSON()
//     const id = item._id
//     const pid = item.pid
//     if (!itemMap[id]) {
//       itemMap[id] = {
//         children: []
//       }
//     }
//     itemMap[id] = {
//       ...item,
//       children: itemMap[id].children
//     }

//     const treeItem = itemMap[id]
//     if (pid === '0') {
//       result.push(treeItem)
//     } else {
//       if (!itemMap[pid]) {
//         itemMap[pid] = {
//           children: []
//         }
//       }
//       itemMap[pid].children.push(treeItem)
//     }
//   }
//   return result
// }

export const toTree = (arr) => {
  const items = arr.map((item) => item.toJSON())
  const result = []
  const map = new Map()

  for (const item of items) {
    map.set(item._id, { ...item, children: [] })
  }

  for (const item of items) {
    if (map.get(item.pid)) {
      map.get(item.pid).children.push(item)
    } else {
      result.push(map.get(item._id))
    }
  }
}

export const getIpAddr = (ctx) => {
  const headers = ctx.headers

  if (headers['x-real-ip']) {
    console.log(headers['x-real-ip'])
    return headers['x-real-ip']
  }
  if (headers['x-forwarded-for']) {
    const ipList = headers['x-forwarded-for'].split(',')
    console.log(ipList)
    return ipList[0]
  }
  return '0.0.0.0'
}
