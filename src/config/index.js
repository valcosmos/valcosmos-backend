
import 'dotenv/config'

export const isDevMode = process.env.NODE_ENV === 'development'

const { DB_HOST, DB_NAME, DB_PORT, DB_USER, DB_PWD } = process.env

export const DB_URL = `mongodb://${DB_USER}:${DB_PWD}@${DB_HOST}:${DB_PORT}/${DB_NAME}`

export const JWT_SECRET = 'privatekey'
