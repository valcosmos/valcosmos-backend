import mongoose from 'mongoose'
import { DB_URL } from '@/config'

mongoose.connect(DB_URL)

mongoose.connection.on('connected', () => {
  console.log('Mongoose connection open to: ==>' + DB_URL)
})

mongoose.connection.on('error', (err) => {
  console.log('Mongoose connection error==>' + err)
})

mongoose.connection.on('disconnected', () => {
  console.log('Mongoose connection disconnected')
})

export default mongoose
