import mongoose from '@/config/mdb'

const Schema = mongoose.Schema

const userSchema = new Schema(
  {
    email: {
      type: String,
      index: { unique: true },
      sparse: true
    },
    password: { type: String },
    gender: {
      type: String,
      default: 'male'
    },
    signature: {
      type: String,
      default: ''
    },
    info: {
      type: String,
      default: ''
    },
    avatar: {
      type: String,
      default: ''
    }
  },
  {
    timestamps: {
      createdAt: 'created',
      updatedAt: 'updated'
    }
  }
)

const User = mongoose.model('users', userSchema)

export default User
