import mongoose from '@/config/mdb'

const Schema = mongoose.Schema

const CommentsSchema = new Schema({
  postId: {
    type: String,
    ref: 'posts'
  },
  nickname: {
    type: String,
    default: ''
  },
  content: {
    type: String,
    default: ''
  },
  pid: {
    type: String,
    default: '0'
  }
}, {
  timestamps: {
    createdAt: 'created',
    updatedAt: 'updated'
  }
})

CommentsSchema.statics = {
  getList (options, sort, current, pageSize) {
    return this.find(options)
      .sort({ created: sort })
      .skip(current * pageSize)
      .limit(pageSize)
  }
}

const Comments = mongoose.model('comments', CommentsSchema)

export default Comments
