import mongoose from '@/config/mdb'

const Schema = mongoose.Schema

const MessagesSchema = new Schema({
  nickname: {
    type: String,
    default: ''
  },
  content: {
    type: String,
    default: ''
  },
  pid: {
    type: String,
    default: '0'
  }
}, {
  timestamps: {
    createdAt: 'created',
    updatedAt: 'updated'
  }
})

MessagesSchema.statics = {
  getList (options, sort, current, pageSize) {
    return this.find(options)
      .sort({ created: sort })
      .skip(current * pageSize)
      .limit(pageSize)
  }
}

const Msg = mongoose.model('messages', MessagesSchema)
export default Msg
