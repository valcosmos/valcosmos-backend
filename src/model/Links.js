import mongoose from '@/config/mdb'

const Schema = mongoose.Schema
const LinkScheme = new Schema(
  {
    nickname: { type: String, default: '' },
    desc: { type: String, default: '' },
    url: { type: String, default: '' },
    avatar: { type: String, default: '' }
  },
  { timestamps: { createdAt: 'created', updatedAt: 'updated' } }
)

LinkScheme.statics = {
  getList (options, sort, current, pageSize) {
    return this.find(options)
      .sort({ created: sort })
      .skip(current * pageSize)
      .limit(pageSize)
  }
}

const Links = mongoose.model('links', LinkScheme)
export default Links
