import mongoose from '@/config/mdb'

const Schema = mongoose.Schema

const postSchema = new Schema(
  {
    uid: { type: String, ref: 'users' },
    title: { type: String },
    content: { type: String },
    catalog: { type: String },
    tags: { type: Array, default: [] },
    isTop: { type: String, default: '0' },
    read: { type: Number, default: '0' },
    like: { type: Number, default: 0 },
    clientIp: { type: String, default: '' },
    comment: { type: Number, default: 0 }
  },
  { timestamps: { createdAt: 'created', updatedAt: 'updated' } }
)

postSchema.statics = {
  getList (options, sort, current, pageSize) {
    return this.find(options)
      .sort({ created: sort })
      .skip(current * pageSize)
      .limit(pageSize)
  },
  getHots () {
    return this.find({}).sort({ read: -1 }).limit(9)
  }
}

const Post = mongoose.model('posts', postSchema)

export default Post
