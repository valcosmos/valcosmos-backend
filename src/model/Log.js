import mongoose from '@/config/mdb'

const LogSchema = new mongoose.Schema({
  log: {
    type: String,
    default: ''
  }
}, {
  timestamps: {
    createdAt: 'created',
    updatedAt: 'updated'
  }
})

LogSchema.statics = {
  getList (options, sort, current, pageSize) {
    return this.find(options).sort({ created: sort }).skip(current * pageSize).limit(pageSize)
  }
}

const Log = mongoose.model('timeline', LogSchema)

export default Log
