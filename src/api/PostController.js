import { getIpAddr } from '@/common/utils'
import Post from '@/model/Post'

class PostController {
  async getPosts (ctx) {
    // for (let i = 1; i <= 60; i++) {
    //   const data = new Post({
    //     title: `pinia${i}`,
    //     catalog: 'frontend',
    //     tags: ['vue'],
    //     like: i,
    //     comment: i + 10,
    //     read: i * 2,
    //     content:
    //       '## 1. md-editor-v3\n\nMarkdown 编辑器，基于 react，使用 jsx 和 typescript 语法开发，支持切换主题、prettier 美化文本等。\n\n### 1.1 基本演示\n\n**加粗**，<u>下划线</u>，_斜体_，~删除线~，上标<sup>26</sup>，下标<sub>[1]</sub>，`inline code`，[超链接](https://imbf.cc)\n\n> 引用：世界上没有绝对，只有相对\n\n## 2. 代码演示\n\n```js\nimport { defineComponent, ref } from "vue";\nimport MdEditor from "md-editor-v3";\nimport "md-editor-v3/lib/style.css";\n\n\nexport default defineComponent({\n  name: "MdEditor",\n  setup() {\n    const text = ref("");\n    return () => (\n      <MdEditor modelValue={text.value} onChange={(v: string) => (text.value = v)} />\n    );\n  }\n});\n```\n\n\n## 3. 文本演示\n\n\n依照普朗克长度这项单位，目前可观测的宇宙的直径估计值（直径约 930 亿光年，即 8.8 × 10<sup>26</sup> 米）即为 5.4 × 10<sup>61</sup>倍普朗克长度。而可观测宇宙体积则为 8.4 × 10<sup>184</sup>立方普朗克长度（普朗克体积）。\n\n\n## 4. 表格演示\n\n\n| 昵称 | 猿龄（年） | 来自      |\n| ---- | ---------- | --------- |\n| 之间 | 3          | 中国-重庆 |\n\n\n## 5. 占个坑@！\n'
    //   })
    //   const res = await data.save()
    //   console.log(res)
    // }
    const params = ctx.query
    const sort = params.sort || -1
    const current = parseInt(params.current) - 1 || 0
    const pageSize = parseInt(params.pageSize) || 10
    const postList = await Post.getList({}, sort, current, pageSize)
    const total = await Post.countDocuments()
    ctx.body = {
      code: 200,
      data: postList,
      msg: '获取文章列表成功',
      total
    }
  }

  async getHotPosts (ctx) {
    // const { body } = ctx.request
    const hots = await Post.getHots()
    const data = hots.map((hot) => {
      const obj = { ...hot.toJSON() }
      delete obj.content
      return obj
    })
    ctx.body = {
      code: 200,
      data: data,
      msg: '获取热点文章成功'
    }
  }

  async setPost (ctx) {
    const { body } = ctx.request
    const post = new Post(body)
    const res = await post.save()
    ctx.body = {
      code: 200,
      data: res,
      msg: '添加文章成功'
    }
  }

  async putPost (ctx) {
    const { body } = ctx.request
    const res = await Post.updateOne({ _id: body._id }, body)
    if (res.modifiedCount === 1) {
      return (ctx.body = {
        code: 200,
        msg: '更新文章成功'
      })
    }
    ctx.body = {
      code: 404,
      msg: '更新文章失败'
    }
  }

  async delPosts (ctx) {
    const params = ctx.query
    const res = await Post.deleteMany({ _id: params.ids })
    if (res.deletedCount) {
      return (ctx.body = {
        code: 200,
        msg: '删除文章成功'
      })
    }
    ctx.body = {
      code: 404,
      msg: '删除文章失败'
    }
  }

  async setLike (ctx) {
    const { body } = ctx.request
    const clientIp = getIpAddr(ctx)
    const ip = await Post.findOne({ _id: body.id, clientIp: clientIp })
    let res
    let msg = ''
    if (ip) {
      res = await Post.updateOne(
        { _id: body.id },
        { $inc: { like: -1 }, $set: { clientIp: '' } }
      )
      msg = '您已取消点赞'
    } else {
      res = await Post.updateOne(
        { _id: body.id },
        { $inc: { like: 1 }, $set: { clientIp } }
      )
      msg = '点赞成功'
    }
    if (res.modifiedCount) {
      const post = await Post.findOne({ _id: body.id })
      return (ctx.body = {
        code: 200,
        data: post.like,
        msg,
        clientIp
      })
    }
    ctx.body = {
      code: 404,
      msg: '点赞失败'
    }
  }

  async getPostDetial (ctx) {
    const params = ctx.query
    const post = await Post.findOne({ _id: params.id })
    if (post) {
      await Post.updateOne({ _id: params.id }, { $inc: { read: 1 } })
      return (ctx.body = {
        code: 200,
        data: post
      })
    }
    ctx.body = {
      code: 404,
      data: post
    }
  }
}

export default new PostController()
