import User from '@/model/User'
import { getJWTPayload } from '@/common/utils'

class UserController {
  async setUser (ctx) {
    const { body } = ctx.request
    const obj = await getJWTPayload(ctx.header.authorization)
    const res = await User.updateOne({ _id: obj._id }, body)
    if (res.modifiedCount) {
      return (ctx.body = {
        code: 200,
        msg: '更新信息成功'
      })
    }
    ctx.body = {
      code: 404,
      msg: '更新信息失败'
    }
  }

  async getUserMsg (ctx) {
    const user = await User.findOne({ email: 'westerngod@126.com' })
    const arr = ['password']
    const newObj = user.toJSON()
    arr.map(item => delete newObj[item])
    ctx.body = {
      code: 200,
      data: newObj,
      msg: '获取信息成功'
    }
  }
}

export default new UserController()
