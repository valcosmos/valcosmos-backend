import Log from '@/model/Log'

class LogController {
  async getLogs (ctx) {
    // for (let i = 0; i < 40; i++) {
    //   const log = new Log({
    //     log: 'test' + i
    //   })
    //   const res = await log.save()
    //   console.log(res)
    // }
    const params = ctx.query
    const sort = params.sort || -1
    const current = +params.current - 1 || 0
    const pageSize = +params.pageSize || 10
    const data = await Log.getList({}, sort, current, pageSize)
    const total = await Log.countDocuments()
    ctx.body = {
      code: 200,
      data,
      total,
      msg: '获取日志成功'
    }
  }

  async setLog (ctx) {
    const { body } = ctx.request
    const log = new Log(body)
    const res = await log.save()
    ctx.body = {
      code: 200,
      msg: '添加日志成功',
      data: res
    }
  }

  async putLog (ctx) {
    const { body } = ctx.request
    const res = await Log.updateOne({ _id: body._id }, body)
    console.log(res)
    if (res.modifiedCount) {
      return (
        ctx.body = {
          code: 200,
          msg: '更新数据成功'
        }
      )
    }
    ctx.body = {
      code: 404,
      msg: '更新数据失败'
    }
  }

  async delLogs (ctx) {
    const params = ctx.query
    const res = await Log.deleteMany({ _id: params.ids })
    if (res.deletedCount) {
      return (ctx.body = {
        code: 200,
        msg: '删除成功'
      })
    }
    ctx.body = {
      code: 404,
      msg: '删除失败'
    }
  }
}

export default new LogController()
