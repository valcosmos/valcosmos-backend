import Links from '@/model/Links'

class LinkController {
  async getLinks (ctx) {
    // for (let i = 0; i < 20; i++) {
    //   const link = new Links({
    //     nickname: 'valcosmos',
    //     desc: 'this is content a a aaaaaa a aaa',
    //     url: 'https://www.valzt.cn',
    //     avatar: '/2022-03-10/2f043f0c-59df-4ee4-90af-0645fb9e490b.png'
    //   })
    //   const res = await link.save()
    //   console.log(res)
    // }
    const params = ctx.query
    const sort = params.sort || -1
    const current = +params.current - 1 || 0
    const pageSize = +params.pageSize || 10
    const data = await Links.getList({}, sort, current, pageSize)
    const total = await Links.countDocuments()
    ctx.body = {
      code: 200,
      msg: '获取友链成功',
      data,
      total
    }
  }

  async setLink (ctx) {
    const { body } = ctx.request
    const links = new Links(body)
    const res = await links.save()
    console.log(
      '🚀 ~ file: LinkController.js ~ line 23 ~ LinkController ~ setLinks ~ res',
      res
    )
    ctx.body = {
      code: 200,
      msg: '添加友链成功'
    }
  }

  async putLink (ctx) {
    const { body } = ctx.request
    console.log(body)
    const res = await Links.updateOne({ _id: body._id }, body)
    if (res.modifiedCount) {
      return (ctx.body = {
        code: 200,
        msg: '更新友链成功'
      })
    }
    ctx.body = {
      code: 404,
      msg: '更新失败'
    }
  }

  async delLinks (ctx) {
    const params = ctx.query
    const res = await Links.deleteMany({ _id: params.ids })
    if (res.deletedCount) {
      return (ctx.body = {
        code: 200,
        msg: '删除友链成功'
      })
    }
    ctx.body = {
      code: 404,
      msg: '删除友链失败'
    }
  }
}

export default new LinkController()
