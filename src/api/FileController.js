import { v4 as uuid } from 'uuid'
import dayjs from 'dayjs'
import path from 'path'
import mkdir from 'make-dir'
import { createReadStream, createWriteStream } from 'fs'

class FileController {
  async uploadImg (ctx) {
    const file = ctx.request.files.file
    const ext = file.name.split('.').pop()
    const dir = `${path.join(__dirname, '../../public')}/${dayjs().format('YYYY-MM-DD')}`
    await mkdir(dir)
    const avatar = uuid()
    const destPath = `${dir}/${avatar}.${ext}`
    const reader = createReadStream(file.path)
    const upStream = createWriteStream(destPath)
    const filePath = `/${dayjs().format('YYYY-MM-DD')}/${avatar}.${ext}`
    reader.pipe(upStream)
    ctx.body = {
      code: 200,
      data: filePath,
      msg: '图片上传成功'
    }
  }

  async uploadAvatar (ctx) {
    const file = ctx.request.files.file
    const ext = file.name.split('.').pop()
    const dir = path.join(__dirname, '../../avatar')
    await mkdir(dir)
    const avatar = dayjs().format('YYYY-MM-DD')
    const destPath = `${dir}/${avatar}.${ext}`
    const reader = createReadStream(file.path)
    const upStream = createWriteStream(destPath)
    const filePath = `/${avatar}.${ext}`
    reader.pipe(upStream)
    ctx.body = {
      code: 200,
      data: filePath,
      msg: '头像上传成功'
    }
  }
}

export default new FileController()
