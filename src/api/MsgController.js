import { toTree } from '@/common/utils'
import Msg from '@/model/Msg'

class MsgController {
  async getMsgs (ctx) {
    // for (let i = 0; i < 40; i++) {
    // let i = 0
    // setInterval(async () => {
    //   i++
    //   const msg = new Msg({
    //     nickname: 'aaa' + i,
    //     content: 'content' + i
    //   })
    //   const res = await msg.save()
    //   console.log(res)
    // }, 1000)
    // }

    // const params = ctx.query
    // const sort = +params.sort || -1
    // const current = +params.current - 1 || 0
    // const pageSize = +params.pageSize || 10

    // const res = await Msg.getList({}, sort, current, pageSize)
    const res = await Msg.find({}).sort({ created: -1 })

    const total = await Msg.countDocuments()
    ctx.body = {
      code: 200,
      data: res,
      total: total,
      msg: '获取留言成功'
    }
  }

  async setMsg (ctx) {
    const { body } = ctx.request
    const msg = new Msg(body)
    const res = await msg.save()
    if (JSON.stringify(res) !== '{}') {
      return (ctx.body = {
        code: 200,
        msg: '添加留言成功'
      })
    }
    ctx.body = {
      code: 404,
      msg: '添加留言失败'
    }
  }

  async putMsg (ctx) {
    const { body } = ctx.request
    const res = await Msg.updateOne({ _id: body._id }, body)
    if (res.modifiedCount) {
      return (ctx.body = {
        code: 200,
        msg: '更新留言成功'
      })
    }
    ctx.body = {
      code: 404,
      msg: '更新留言失败'
    }
  }

  async delMsgs (ctx) {
    const params = ctx.query
    if (params.ids.length <= 0) {
      return (ctx.body = {
        code: 404,
        msg: '删除失败'
      })
    }
    const res = await Msg.deleteMany({ _id: params.ids })
    if (res.deletedCount) {
      return (ctx.body = {
        code: 200,
        msg: '删除成功'
      })
    }
    ctx.body = {
      code: 404,
      msg: '删除失败'
    }
  }
}

export default new MsgController()
