import User from '@/model/User'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import { JWT_SECRET } from '@/config'

const generateToken = (payload, expire = '1h') => {
  if (payload) {
    return jwt.sign({ ...payload }, JWT_SECRET, { expiresIn: expire })
  }
}

class LoginController {
  async login (ctx) {
    const { body } = ctx.request
    let check = false
    const user = await User.findOne({ email: body.email })
    if (!user) {
      return (ctx.body = {
        code: 404,
        msg: '用户名不存在'
      })
    }
    if (await bcrypt.compare(body.password, user.password)) {
      check = true
    } else {
      return (ctx.body = {
        code: 404,
        msg: '密码错误'
      })
    }
    if (check) {
      const userObj = user.toJSON()
      const arr = ['password']
      arr.map((item) => delete userObj[item])
      return (ctx.body = {
        code: 200,
        data: userObj,
        token: generateToken({ _id: userObj._id }, '60m')
      })
    }
    ctx.body = {
      code: 500,
      msg: 'unknown error'
    }
  }

  async reg (ctx) {
    const { body } = ctx.request
    const isExistUser = await User.findOne({ email: body.email })
    let msg = ''
    let reg = true
    if (isExistUser !== null && typeof isExistUser.email !== 'undefined') {
      msg = '邮箱已经被注册，可以通过邮箱找回'
      reg = false
    }
    if (reg) {
      body.password = await bcrypt.hash(body.password, 5)
      const user = new User({
        email: body.email,
        password: body.password
      })
      const res = await user.save()
      console.log('🚀 ~ file: LoginController.js ~ line 24 ~ LoginController ~ reg ~ res', res)
      return (ctx.body = {
        code: 200,
        data: res,
        msg: '注册成功'
      })
    }
    ctx.body = {
      code: 500,
      msg
    }
  }
}

export default new LoginController()
