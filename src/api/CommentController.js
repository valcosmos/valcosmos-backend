import Comments from '@/model/Comments'

class CommentController {
  async getComments (ctx) {
    // for (let i = 0; i < 40; i++) {
    //   const data = new Comments({
    //     nickname: 'nickname' + i,
    //     content: 'content1' + i
    //   })

    //   const res = await data.save()
    //   console.log(res)
    // }
    const params = ctx.query
    const postId = params.postId
    // const sort = params.sort || -1
    // const current = +params.current - 1 || 0
    // const pageSize = +params.pageSize || 10

    // const data = await Comments.getList({}, sort, current, pageSize)
    const data = await Comments.find({ postId: postId })
    console.log(postId)
    const total = data.length

    ctx.body = {
      code: 200,
      data,
      total,
      msg: '获取评论成功'
    }
  }

  async setComment (ctx) {
    const { body } = ctx.request
    const comment = new Comments(body)
    const res = await comment.save()
    if (JSON.stringify(res) !== '{}') {
      return (ctx.body = {
        code: 200,
        msg: '评论成功'
      })
    }
    ctx.body = {
      code: 404,
      msg: '评论失败'
    }
  }

  async putComment (ctx) {}

  async delComments (ctx) {
    const params = ctx.query
    const res = await Comments.deleteMany({ _id: params.ids })
    console.log(res)
    if (res.deletedCount) {
      ctx.body = {
        code: 200,
        msg: '删除成功'
      }
    }
  }
}

export default new CommentController()
