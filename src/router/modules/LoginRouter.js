import LoginController from '@/api/LoginController'
import Router from 'koa-router'

const router = new Router()

router.prefix('/auth')

router.post('/login', LoginController.login)

router.post('/register', LoginController.reg)

export default router
