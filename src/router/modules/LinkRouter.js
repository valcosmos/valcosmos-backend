import FileController from '@/api/FileController'
import Router from 'koa-router'
import LinkController from '@/api/LinkController'

const router = new Router()

router.post('/upload-img', FileController.uploadImg)

router.post('/set-link', LinkController.setLink)

router.put('/put-link', LinkController.putLink)

router.del('/del-links', LinkController.delLinks)

export default router
