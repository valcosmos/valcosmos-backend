import CommentController from '@/api/CommentController'
import Router from 'koa-router'

const router = new Router()

router.del('/del-comments', CommentController.delComments)

router.put('/put-comment', CommentController.putComment)

export default router
