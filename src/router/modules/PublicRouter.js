import CommentController from '@/api/CommentController'
import LinkController from '@/api/LinkController'
import PostController from '@/api/PostController'
import LogController from '@/api/LogController'
import UserController from '@/api/UserController'
import Router from 'koa-router'
import MsgController from '@/api/MsgController'

const router = new Router()

router.prefix('/public')

router.get('/get-posts', PostController.getPosts)

router.get('/get-hot-posts', PostController.getHotPosts)

router.get('/get-post-detail', PostController.getPostDetial)

router.post('/set-like', PostController.setLike)

router.get('/get-comments', CommentController.getComments)

router.post('/set-comment', CommentController.setComment)

router.get('/get-links', LinkController.getLinks)

router.get('/get-logs', LogController.getLogs)

router.get('/get-msgs', MsgController.getMsgs)

router.post('/set-msg', MsgController.setMsg)

router.get('/get-user-msg', UserController.getUserMsg)

export default router
