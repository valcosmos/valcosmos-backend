import PostController from '@/api/PostController'
import Router from 'koa-router'

const router = new Router()

router.post('/set-post', PostController.setPost)

router.put('/put-post', PostController.putPost)

router.del('/del-posts', PostController.delPosts)

export default router
