import Router from 'koa-router'

import LogController from '@/api/LogController'

const router = new Router()

router.post('/set-log', LogController.setLog)

router.put('/put-log', LogController.putLog)

router.del('/del-logs', LogController.delLogs)

export default router
