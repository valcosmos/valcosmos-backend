import Router from 'koa-router'
import UserController from '@/api/UserController'
import FileController from '@/api/FileController'

const router = new Router()

router.post('/set-user-info', UserController.setUser)

router.post('/upload-avatar', FileController.uploadAvatar)

export default router
