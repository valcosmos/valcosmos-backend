import Router from 'koa-router'
import MsgController from '@/api/MsgController'

const router = new Router()

router.put('/put-msg', MsgController.putMsg)

router.del('/del-msgs', MsgController.delMsgs)

export default router
