import combineRouters from 'koa-combine-routers'

const moduleFiles = require.context('@/router/modules', true, /\.js$/)

const modules = moduleFiles.keys().reduce((items, path) => {
  // console.log('==>', items)
  // console.log('==>', path)
  const value = moduleFiles(path)
  items.push(value.default)
  return items
}, [])


export default combineRouters(modules)
