import Koa from 'koa'
import koaBody from 'koa-body'
import compose from 'koa-compose'
import compress from 'koa-compress'
import helmet from 'koa-helmet'
import router from '@/router/index'
import cors from '@koa/cors'
import statics from 'koa-static'
import path from 'path'
import JWT from 'koa-jwt'
import { isDevMode, JWT_SECRET } from '@/config'
import errorHandle from '@/common/errorHandle'

const app = new Koa()

const jwt = JWT({ secret: JWT_SECRET }).unless({
  path: [/^\/public/, /^\/auth/]
})

const middleware = compose([
  koaBody({
    multipart: true,
    formidable: {
      keepExtensions: true,
      maxFieldsSize: 5 * 1024 * 1024
    },
    onError: (err) => {
      console.log('koaBody TCL: err', err)
    }
  }),
  statics(path.join(__dirname, '../public')),
  statics(path.join(__dirname, '../avatar')),
  cors(),
  helmet(),
  jwt,
  errorHandle
])
if (!isDevMode) {
  app.use(compress())
}
app.use(middleware)
app.use(router())

app.listen(3000, () => {
  console.log('===> App running at: http://localhost:3000')
})
