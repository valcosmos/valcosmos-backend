// import path from 'path'
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const path = require('path')
const { DefinePlugin } = require('webpack')
const nodeExternals = require('webpack-node-externals')

const resolve = dir => path.join(__dirname, '..', dir)

const p = process.versions.node
console.log(p)

const APP_PATH = resolve('src')
const DIST_PATH = resolve('dist')

const webpackConfigBase = {
  target: 'node',
  entry: {
    server: path.join(APP_PATH, 'main.js')
  },
  resolve: {
    modules: [APP_PATH, 'node_modules'],
    extensions: ['.js', '.json'],
    alias: {
      '@': APP_PATH
    }
  },
  output: {
    filename: '[name].bundle.js',
    path: DIST_PATH
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: { loader: 'babel-loader' },
        exclude: [path.join(__dirname, '/node_modules')]
      }
    ]
  },
  externals: [nodeExternals()],
  plugins: [
    new CleanWebpackPlugin(),
    new DefinePlugin({
      'process.env.NODE_ENV': process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'prod' ? JSON.stringify('production') : JSON.stringify('development')
    })
  ],
  node: {
    global: true,
    __dirname: true,
    __filename: true
  }
}

module.exports = webpackConfigBase
