const { merge } = require('webpack-merge')

const base = require('./webpack.config.base')

const webpackConfig = merge(base, {
  mode: 'development',
  devtool: 'eval-cheap-source-map',
  stats: { children: false, errorDetails: true }
})

module.exports = webpackConfig
