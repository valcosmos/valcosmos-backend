FROM node:16-alpine

WORKDIR /root/app

COPY . .

RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.tuna.tsinghua.edu.cn/g' /etc/apk/repositories && apk update

RUN apk add python3

RUN ln -s /usr/bin/python3 /usr/bin/python

RUN apk --no-cache add --virtual builds-deps build-base alpine-sdk

RUN npm config set registry https://registry.npm.taobao.org

RUN npm i -g pnpm

RUN pnpm config set registry https://registry.npm.taobao.org

RUN pnpm i

RUN pnpm build

EXPOSE 3000

VOLUME [ "/root/app/public" ]

CMD [ "node", "dist/server.bundle.js" ]